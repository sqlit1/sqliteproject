/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nut.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Win 10 Home
 */
public class CreateTable {
        public static void main(String[] args) throws SQLException {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        //Connection
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = conn.createStatement();
            String sql = "CREATE TABLE user (\n"
                    + "    id       INTEGER    PRIMARY KEY ASC AUTOINCREMENT,\n"
                    + "    username CHAR (16)  UNIQUE,\n"
                    + "    password CHAR (255) \n"
                    + ");";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();

        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connect database!!!");
            System.exit(0);
        }
    }

}
